#include "InputSystem.hpp"


namespace Tyr
{
	InputSystem::InputSystem()
	{
	}


	InputSystem::~InputSystem()
	{
	}

	void InputSystem::Update(float delta_time)
	{
		// delta time is not really that important here
	}

	bool InputSystem::GetKey(const_string_ref key)
	{
		auto iter = keys_.find(key);
		if (iter != keys_.end())
		{
			return iter->second;
		}
		return false;
	}

	bool InputSystem::GetKeyByAlias(const_string_ref alias)
	{
		auto iter = key_aliases_.find(alias);
		if (iter != key_aliases_.end())
		{
			return GetKey(iter->second);
		}
		return false;
	}
	bool InputSystem::GetMouseButton(const_string_ref button)
	{
		return false;
	}
	void InputSystem::AddKeyAlias(const_string_ref alias, const_string_ref key)
	{	
		key_aliases_.emplace(alias, key);
	}
	void InputSystem::AddMouseAlias(const_string_ref alias, const_string_ref key)
	{
		mouse_button_aliases_.emplace(alias, key);
	}
}