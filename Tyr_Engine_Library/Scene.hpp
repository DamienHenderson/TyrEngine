#pragma once
#include <GL\glew.h>

#include "Common.h"

#include "Entity.hpp"
#include "GL_Shader.hpp"
#include "Light.hpp"

#include "TransformComponent.hpp"
#include "RenderableComponent.hpp"

#include "RenderingSystem.hpp"

#include "SceneNode.hpp"

class Scene
{
public:
	
	Scene();
	virtual ~Scene();
	void    AddEntity(const std::string& identifier, std::shared_ptr<Entity>& entity);
	void    AddEntity(std::shared_ptr<Tyr::SceneNode>& parent, const std::string& identifier, std::shared_ptr<Entity>& entity);
	Entity& GetEntity(const std::string& identifier);
	std::shared_ptr<Tyr::SceneNode> GetNode(const std::string& identifier);


	void         AddPointLight(const PointLight& light);
	PointLight&  GetPointLight(int idx);
	u32          GetPointLightCount() const { return point_lights_.size(); }

	void         AddDirLight(const DirLight& light);
	DirLight&    GetDirLight(int idx);
	u32          GetDirLightCount() const { return directional_lights_.size(); }

	void         AddSpotLight(const SpotLight& light);
	SpotLight&   GetSpotLight(int idx);
	u32          GetSpotLightCount() const { return spot_lights_.size(); }


	void    Render(Tyr::RenderingSystem& renderer, GL_Shader& shader);
	u32     GetEntityCount();

	const std::vector<std::string>& GetEntityNames() const;
private:
	// Should i use a SceneGraph/Tree for this?
	// std::map<std::string, Entity> entities_;
	
	void Render(std::shared_ptr<Tyr::SceneNode>& node, Tyr::RenderingSystem& renderer, GL_Shader& shader, const glm::mat4& parent_transform);
	std::vector<PointLight>  point_lights_;
	std::vector<DirLight>    directional_lights_;
	std::vector<SpotLight>   spot_lights_;

	std::vector<std::string> entity_names_;

	const u32 max_point_lights_in_pass{ 4 };

	// TODO: implement scene graph/tree
	// Q: should this be a seperate class or should i implement this in the scene?
	// for now use just the root node in the scene
	std::shared_ptr<Tyr::SceneNode> root_node_;
};

