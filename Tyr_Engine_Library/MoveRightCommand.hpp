#pragma once
#include "Command.hpp"
class MoveRightCommand : public Command
{
public:
	MoveRightCommand(Entity* entity);
	~MoveRightCommand();

	virtual void execute();

	virtual void undo();
};

