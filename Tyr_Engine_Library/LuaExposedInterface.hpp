#pragma once

#include <iostream>

class LuaExposedInterface
{
public:
	LuaExposedInterface()
	{

	}
	virtual  ~LuaExposedInterface()
	{

	}
private:
	virtual void expose_to_lua()
	{
		std::cout << "LuaExposedInterface: This is a base class there is no implementation of this function" << std::endl;
	}
};