#include "MoveXYZCommand.hpp"




MoveXYZCommand::MoveXYZCommand(Entity * entity, glm::vec3 new_pos) : Command(entity), destination(new_pos)
{
	previous = entity->GetComponent<TransformComponent>()->GetTranslation();
}

MoveXYZCommand::~MoveXYZCommand()
{
}

void MoveXYZCommand::execute()
{
	entity_->GetComponent<TransformComponent>()->SetTranslation(destination);
}

void MoveXYZCommand::undo()
{
	entity_->GetComponent<TransformComponent>()->SetTranslation(-destination);
}
