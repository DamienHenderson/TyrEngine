#pragma once

#include <GL\glew.h>

#include <glm\glm.hpp>


#include "Common.h"
#include "Types.hpp"
#include "GL_Shader.hpp"
#include "Utils.hpp"

GLint TextureFromFile(const std::string& path, const std::string& directory);
GLint sRGBA_TextureFromFile(const std::string& path, const std::string& directory);
void GLErrorCheck();

void glUniformVec3(GL_Shader& shader,GLint location ,const glm::vec3 uniform);

