#include "TransformComponent.hpp"





TransformComponent::TransformComponent(Entity* parent,const glm::mat4& initial_transform) : Component(parent), transform_(initial_transform)
{
	
}

TransformComponent::~TransformComponent()
{
}

std::string TransformComponent::GetDebugString() const
{
	return "Transform Component";
}

glm::mat4 & TransformComponent::GetTransform()
{
	return transform_;
}

void TransformComponent::SetTransform(const glm::mat4 & new_value)
{
	transform_ = new_value;
}

void TransformComponent::SetScale(const glm::vec3 & value)
{
	transform_ = glm::scale(transform_, value);
}

void TransformComponent::SetRotation(const glm::vec3 & value)
{
	transform_ = glm::rotate(transform_ , 1.0f , value);
}

void TransformComponent::SetTranslation(const glm::vec3 & value)
{
	transform_ = glm::translate(transform_, value);
}

glm::vec3 TransformComponent::GetTranslation()
{
	glm::vec3 translation , scale,skew;
	glm::quat orientation;
	glm::vec4 perspective;

	glm::mat4 transform_tmp = transform_;
	glm::decompose(transform_tmp, scale, orientation, translation, skew, perspective);

	
	return translation;
}
