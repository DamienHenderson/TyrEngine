#pragma once

#include <GL/glew.h>

#include "Common.h"

#include "CubemapTexture.hpp"
#include "GL_Shader.hpp"
#include "Camera.hpp"

class Skybox
{
public:
	Skybox(const std::string& dir, const std::string& PosX, const std::string& NegX, const std::string& PosY, const std::string& NegY, const std::string& PosZ, const std::string& NegZ);
	~Skybox();
	void Render(GL_Shader& skybox_shader,Camera& camera,const glm::mat4& projection);

	/// Sets up the OpenGL Rendering data for the skybox 
	/// Called by the Constructor
	void Load();
private:
	CubemapTexture cubemap;

	// Setup skybox VAO
	GLuint skyboxVAO = 0, skyboxVBO = 0;
	

	GLfloat skyboxVertices[108] = {
		// Positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};
	
};

