#include "CubemapTexture.hpp"

#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


CubemapTexture::CubemapTexture(const std::string & dir, const std::string & PosX, const std::string & NegX, const std::string & PosY, const std::string & NegY, const std::string & PosZ, const std::string & NegZ)
{
	file_names[0] = dir + PosX;
	file_names[1] = dir + NegX;
	file_names[2] = dir + PosY;
	file_names[3] = dir + NegY;
	file_names[4] = dir + PosZ;
	file_names[5] = dir + NegZ;

	Load();
}

CubemapTexture::~CubemapTexture()
{
}

bool CubemapTexture::Load()
{
	
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id);
	glActiveTexture(GL_TEXTURE0);

	for (int i = 0; i < 6; i++)
	{
		std::cout << file_names[i] << std::endl;
		u32 width, height, comp;
		u8* pixels = stbi_load(file_names[i].c_str(), (int*)&width, (int*)&height, (int*)&comp, 4);
		


		glTexImage2D(types[i], 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	return true;
}

void CubemapTexture::Bind(GLenum texture_unit)
{
	glActiveTexture(texture_unit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id);
}
