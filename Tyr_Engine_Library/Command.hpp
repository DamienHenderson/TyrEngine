#pragma once

#include "Common.h"

#include "Entity.hpp"
class Entity;

class Command
{
public:
	Command(Entity* entity_affected);
	~Command();

	virtual void execute();

	virtual void undo();

protected:
	Entity* entity_;
	
};

