#pragma once

#include <string>

#define BIT(n) 1 << n
#include "Types.hpp"
namespace Tyr
{
	struct ImageData
	{
		~ImageData();
		u8* pixels;
		u32 width, height, comp;

	};

	template <typename T>
	void SafeRelease(T* ptr)
	{
		if (ptr != nullptr)
		{
			delete ptr;
		}
	}
	template <typename T>
	void SafeReleaseArray(T* array_ptr)
	{
		if (array_ptr != nullptr)
		{
			delete[] array_ptr;
		}
	}
	template <typename T>
	bool IsSafe(T* ptr)
	{
		return ptr != nullptr;
	}

	ImageData ImageFromFile(const std::string& path);

	using const_string_ref = const std::string&;

	float constant_motion(float start, float speed, float time);

}
