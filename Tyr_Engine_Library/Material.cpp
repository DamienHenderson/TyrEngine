#include "Material.hpp"





Material::Material(const glm::vec3 & diffuse, const glm::vec3 & ambient, const glm::vec3 & specular, float shininess) : diffuse_(diffuse) , ambient_(ambient) , specular_(specular) , shininess_(shininess)
{
}

Material::~Material()
{
}

void Material::ApplyToShader(GL_Shader & shader) const
{
	shader.SetUniform("diffuse", diffuse_);
	shader.SetUniform("ambient", ambient_);
	shader.SetUniform("specular", specular_);
	shader.SetUniform("shininess", shininess_);
	std::cout << "Called base material applytoshader" << std::endl;
}
