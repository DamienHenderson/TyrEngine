#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Common.h"
#include "System.hpp"
#include "Entity.hpp"

namespace Tyr
{
	class RenderingSystem : public Tyr::System
	{
	public:
		RenderingSystem();
		~RenderingSystem();
		virtual void Update(float delta_time) override;

		void Render(Entity& entity,GL_Shader& shader, const glm::mat4& parent_transform) const ;
	protected:
		
	};
}
