#pragma once
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <typeindex>
#include <typeinfo>

#include "Common.h"
#include "Types.hpp"
#include "Component.hpp"
#include "RenderableComponent.hpp"
#include "TransformComponent.hpp"
class Entity
{

public:
	Entity();
	
	Entity(Entity& other);
	virtual ~Entity();
	u32 getInstanceID();
	
	template <typename T, typename... Args>
	void AddComponent(Args&&... component_args);

	template <typename T>
	std::shared_ptr<T> GetComponent();
	template <typename T>
	bool HasComponent() const ;

	template <typename T>
	void RemoveComponent();
	

	void SetTransform(const glm::mat4& new_transform)
	{
		transform_ = new_transform;
	}
	const glm::mat4& GetTransform() const
	{
		return transform_;
	}
private:

	u32 instance_id;

	std::map<ComponentType, std::shared_ptr<Component> > components_;

	void AddComponent(const Component& component);

	glm::mat4 transform_;

	 
};

template<typename T, typename ...Args>
void Entity::AddComponent(Args && ...component_args)
{
	static_assert(std::is_base_of<Component, T>(), "Type must be Derived from Component");
	auto& result(std::make_shared<T>(this,std::forward<Args>(component_args)...));
	components_.emplace(result->Type, result);
}

template <typename T>
std::shared_ptr<T> Entity::GetComponent()
{
	static_assert(std::is_base_of<Component, T>(), "Type must be Derived from Component");
	
	
	
	if (components_.find(T::Type) != components_.end())
	{
		return std::static_pointer_cast<T>(components_[T::Type]);
		
	}
	else
	{
		return std::shared_ptr<T>(nullptr);
	}
}

template<typename T>
inline bool Entity::HasComponent() const
{
	static_assert(std::is_base_of<Component, T>(), "Type must be Derived from Component");
	auto& iter = components_.find(T::Type);
	assert(iter->second);
	return  iter != components_.end();
}

template<typename T>
inline void Entity::RemoveComponent()
{
	if (HasComponent<T>())
	{
		auto& iter = components_[T::Type];

		if (iter != nullptr)
		{
			// release memory here
			iter.reset();
		}
		components_.erase(T::Type);
	
	}
}

