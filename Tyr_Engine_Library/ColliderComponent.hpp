#pragma once
#include <glm/glm.hpp>
#include "Common.h"
#include "Component.hpp"
#include <ReactPhysics3D/reactphysics3d.h>

class ColliderComponent : public Component
{
public:
	

	ColliderComponent(Entity* parent, rp3d::Transform& transform,rp3d::CollisionWorld& collision_world) : Component(parent)
	{
		bounding_volume_ = std::unique_ptr<rp3d::CollisionBody>(collision_world.createCollisionBody(transform));
		bounding_volume_->addCollisionShape(new rp3d::BoxShape(rp3d::Vector3(0.5f, 0.5f, 0.5f)),transform);
	}
	virtual ~ColliderComponent();
	
	bool Intersects(ColliderComponent* other);
	bool Contains(const glm::vec3& point);

	rp3d::CollisionBody* GetCollisionBody() { return bounding_volume_.get(); }

	virtual std::string GetDebugString() const override;

	static constexpr auto Type = ComponentType::Collider;

	virtual ComponentType GetType() const override { return Type; }
private:
	std::unique_ptr<rp3d::CollisionBody> bounding_volume_;
	


};

