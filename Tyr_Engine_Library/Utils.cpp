#include "Utils.hpp"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include <stb_image.h>

namespace Tyr
{
	ImageData::~ImageData()
	{
		// deallocating the image data seems to cause a crash in nvoglv.dll
		// stbi_image_free(pixels);
		// SafeReleaseArray(pixels);
		
	}
	ImageData ImageFromFile(const std::string & path)
	{
		ImageData data;

		data.pixels = stbi_load(path.c_str(), (int*)&data.width, (int*)&data.height, (int*)&data.comp, 4);

		assert(data.pixels != nullptr);
		return data;
	}
	float constant_motion(float start, float speed, float time)
	{
		return start + speed * time;
	}
}