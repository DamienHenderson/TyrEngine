#pragma once
#include "Component.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
class TransformComponent : public Component
{
public:
	
	TransformComponent(Entity* parent,const glm::mat4& initial_transform);
	~TransformComponent();
	virtual std::string GetDebugString() const override;
	virtual TransformComponent* Clone()
	{
		return new TransformComponent(*this);
	}
	glm::mat4& GetTransform();
	void       SetTransform(const glm::mat4 & new_value);
	void       SetScale(const glm::vec3& value);
	void       SetRotation(const glm::vec3& value);
	void       SetTranslation(const glm::vec3& value);

	glm::vec3  GetTranslation();
	static constexpr auto Type = ComponentType::Transform;
	virtual ComponentType GetType() const override { return Type; };
	
private:
	glm::mat4 transform_;
};

