#pragma once
#include "Component.hpp"
#include "Common.h"
#include "Model.hpp"
#include "Entity.hpp"
class Entity;

class RenderableComponent : public Component
{
public:
	
	RenderableComponent(Entity* parent,const std::string& model_filename);
	~RenderableComponent();
	void Render(GL_Shader& shader);

	virtual RenderableComponent* Clone()
	{
		return new RenderableComponent(*this);
	}
	static constexpr auto Type = ComponentType::Renderable;
	virtual ComponentType GetType() const override { return Type; };

private:
	Model model_;

};

