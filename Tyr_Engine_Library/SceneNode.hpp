#pragma once

#include "Common.h"
#include "Entity.hpp"


namespace Tyr
{
	class SceneNode
	{
	public:
		SceneNode(const std::string& name, std::shared_ptr<Entity>& entity);
		~SceneNode();
		std::weak_ptr<Entity> GetEntityPtr() const { return std::weak_ptr<Entity>(entity_); }
		Entity& GetEntity() 
		{ 
		    assert(entity_ != nullptr);
		    return *entity_; 
		}
		std::string GetName() const { return name_; }
		void AddChild(const std::string& name, std::shared_ptr<Entity>& entity);

		std::shared_ptr<SceneNode>& GetChild(const std::string& name,bool recurse = false) 
		{
			if (!recurse)
			{
				auto& iter = children_.find(name);
				if (iter != children_.end())
				{
					return iter->second;
				}
				
			}
			else
			{
				for (auto& iter : children_)
				{
					if (iter.first == name)
					{
						return iter.second;
					}
					else
					{
						auto& recurse_check = iter.second->GetChild(name, recurse);
					}
				}
			}
			return std::shared_ptr<SceneNode>(nullptr);
			
		}

		bool HasChildWithName(const std::string& name, bool recurse = false);
		bool HasChild() const { return children_.size() > 0; }
		std::map<std::string, std::shared_ptr<SceneNode>>& GetChildren();
	private:
		std::map<std::string,std::shared_ptr<SceneNode>> children_;
		std::shared_ptr<Entity> entity_{ nullptr };
		std::string name_;
	};
}