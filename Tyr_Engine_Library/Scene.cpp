#include "Scene.hpp"



Scene::Scene()
{
}


Scene::~Scene()
{
	
}

void Scene::AddEntity(const std::string& identifier, std::shared_ptr<Entity>&  entity)
{
	// entities_.insert(std::make_pair(identifier,std::make_shared<Entity>(entity)));
	
	if (root_node_ == nullptr)
	{
		root_node_ = std::make_shared<Tyr::SceneNode>(identifier, entity);
	}
	else
	{
		root_node_->AddChild(identifier, entity);
	}
	
	entity_names_.push_back(identifier);
}

void Scene::AddEntity(std::shared_ptr<Tyr::SceneNode>& parent, const std::string & identifier, std::shared_ptr<Entity> & entity)
{
	parent->AddChild(identifier, entity);
}



Entity & Scene::GetEntity(const std::string& identifier)
{

	if (identifier == root_node_->GetName())
	{
		return root_node_->GetEntity();
	}
	else if(root_node_->HasChildWithName(identifier,true))
	{
		return root_node_->GetChild(identifier, true)->GetEntity();
	}
	else
	{
		assert("Entity doesn't exist in scene" && false);
		return Entity();
	}
}

std::shared_ptr<Tyr::SceneNode> Scene::GetNode(const std::string & identifier)
{
	if (root_node_ == nullptr)
	{
		return nullptr;
	}
	if (identifier == root_node_->GetName())
	{
		return root_node_;
	}
	else if (root_node_->HasChildWithName(identifier, true))
	{
		return root_node_->GetChild(identifier, true);
	}
	else
	{
		assert("Node doesn't exist in scene" && false);
		return nullptr;
	}

}

void Scene::AddPointLight(const PointLight & light)
{
	point_lights_.push_back(light);
}

PointLight & Scene::GetPointLight(int idx)
{
	return point_lights_[idx];
}

void Scene::AddDirLight(const DirLight & light)
{
	directional_lights_.push_back(light);
}

DirLight & Scene::GetDirLight(int idx)
{
	return directional_lights_[idx];
}

void Scene::AddSpotLight(const SpotLight & light)
{
	spot_lights_.push_back(light);
}

SpotLight & Scene::GetSpotLight(int idx)
{
	return spot_lights_[idx];
}



void Scene::Render(Tyr::RenderingSystem& renderer,GL_Shader & shader)
{
	// TODO: Instanced Rendering
	
// 	for (auto& entity : entities_)
	// {
		
		
		

	// }
	if (root_node_ == nullptr)
	{
		std::cout << "root node is nullptr\n";
		return;
	}
	// std::cout << "Rendering scene\n";
	Render(root_node_, renderer, shader,glm::mat4());
	
}

u32 Scene::GetEntityCount()
{
	return 0;
}

const std::vector<std::string>& Scene::GetEntityNames() const
{
	return entity_names_;
}

void Scene::Render(std::shared_ptr<Tyr::SceneNode>& node, Tyr::RenderingSystem & renderer, GL_Shader & shader, const glm::mat4& parent_transform)
{
	if (node == nullptr)
	{
		return;
	}
	// std::cout << "Rendering entity with name: " << node->GetName() << "\n";
	if (directional_lights_.size() > 0)
	{
		// shader.SetUniform("directional_light", directional_lights_[0]);
	}

	for (u32 i = 0; i < point_lights_.size() && i < max_point_lights_in_pass; i++)
	{
		shader.SetUniform("point_lights[" + std::to_string(i) + "]", point_lights_[i]);
	}
	if (spot_lights_.size() > 0)
	{
		shader.SetUniform("spot_light", spot_lights_[0]);
	}
	// renderer.Render(node->GetEntity(), shader);
	// std::cout << node->GetName() << "\n";
	auto& entity = node->GetEntityPtr();
	if (!entity.expired())
	{
		auto& node_strong_ref = entity.lock();
		renderer.Render(*node_strong_ref, shader, parent_transform);
	}
	
	auto& entities = node->GetChildren();
	for (auto& iter : entities)
	{
		/// Double Rendering here because I'm an idiot
		// auto& entity = iter.second->GetEntity();
		// if (entity.HasComponent<RenderableComponent>())
		// {
		// 	renderer.Render(entity, shader);
		// }
		
		Render(iter.second, renderer, shader,node->GetEntity().GetTransform());
	}
}