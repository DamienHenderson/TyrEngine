#include "ControllerComponent.hpp"



ControllerComponent::ControllerComponent(Entity* parent) : Component(parent)
{
	
}


ControllerComponent::~ControllerComponent()
{
}

void ControllerComponent::ProcessInput(u32 action)
{
	process_action(action);

		
	
}

void ControllerComponent::SetAction(Command * command, u32 action)
{
	if (command != nullptr)
	{
		control_actions_array_[(int)action] = command;
	}
	
}



void ControllerComponent::process_action(u32 action)
{
	// process input here
	if (control_actions_array_[(int)action] != nullptr)
	{
		control_actions_array_[(int)action]->execute();
	}
	
}


