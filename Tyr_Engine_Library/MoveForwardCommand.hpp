#pragma once
#include "Command.hpp"
class MoveForwardCommand : public Command
{
public:
	MoveForwardCommand(Entity* entity);
	~MoveForwardCommand();

	virtual void execute();

	virtual void undo();

};

