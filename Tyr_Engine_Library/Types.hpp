#pragma once

#include <cstdint>
#include <string>
#include <GL\glew.h>
#include <glm\glm.hpp>

/// Unsigned fixed size integer types and structs
using u8   =  uint8_t;
using u16  =  uint16_t;
using u32  =  uint32_t;
using u64  =  uint64_t;
using uptr =  uintptr_t;

struct vec2ui_8
{
	u8 x, y;
};

struct vec2ui_16
{
	u16 x, y;
};

struct vec2ui_32
{
	u32 x, y;
};
struct vec2ui_64
{
	u64 x, y;
};


/// Signed fixed length integer types and structs
using s8   =  int8_t;
using s16  =  int16_t;
using s32  =  int32_t;
using s64  =  int64_t;
using sptr =  intptr_t;

struct vec2i_8
{
	s8 x, y;
};

struct vec2i_16
{
	s16 x, y;
};

struct vec2i_32
{
	s32 x, y;
};
struct vec2i_64
{
	s64 x, y;
};

struct GL_Vertex
{
	
	glm::vec3 position, normal;
	glm::vec2 uv;
	glm::vec3 tangent, bitangent;
	
	
};

struct Texture 
{
	GLuint id;
	std::string type;
	std::string path;
};




