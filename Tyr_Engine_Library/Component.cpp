#include "Component.hpp"



Component::Component(Entity* parent_entity) : parent_entity_(parent_entity)
{

}

Component::Component(const Component & to_copy)
{
	this->parent_entity_ = to_copy.parent_entity_;
}

Component::~Component()
{
}

std::string Component::GetDebugString() const
{
	return "Base Component.";
}

ComponentType Component::GetType() const
{
	return Type;
}



void Component::SetParent(Entity * new_parent)
{
	if (new_parent != nullptr)
	{
		parent_entity_ = new_parent;
	}
}

