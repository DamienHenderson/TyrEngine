#include "MapFile.hpp"

void write_MAP(const std::string& filename, const std::vector<u32>& map_data, u32 width, u32 height)
{
	std::ofstream file(filename, std::ios::out | std::ios::binary);

	MAPFileHeader header;
	header.width = width;
	header.height = height;
	// header.Tiledefs_file = "default.tiledef";

	file.write((char *)&header, sizeof(MAPFileHeader));

	for (u32 row = 0; row < height; row++)
	{
		for (u32 column = 0; column < width; column++)
		{
			u32 value = map_data[column + (row * width)];

			file.write((char*)&value, sizeof(u32));
		}
	}

	file.close();
}

std::vector<std::vector<u32>> read_MAP(const std::string & filename)
{
	std::ifstream infile(filename, std::ios::in | std::ios::binary);

	std::vector<std::vector<u32>> outvec;

	MAPFileHeader header;

	infile.read((char*)&header, sizeof(MAPFileHeader));

	std::cout << header.identifier << " " << header.width << " " << header.height << std::endl;
	
	
	if (std::string(header.identifier) != "MAP")
	{
	 	// throw new std::runtime_error("File Invalid: Header does not match expected");
	 	return std::vector<std::vector<u32>>();
	}
	else
	{
		
		
		// width = (u32)width_c;
		// height = (u32)height_c;
		outvec.resize(header.width * header.height);
		std::cout << header.width << " " << header.height << std::endl;
		u32 temp = 0;
		for (u32 row = 0; row < header.height; row++)
		{
			for (u32 column = 0; column < header.width; column++)
			{
				u32 value;
				
				infile.read((char*)&value, sizeof(u32));
				

				outvec[row].push_back(value);
			}
		}
		return outvec;
		infile.close();
	}
	infile.close();
	//return std::vector<std::vector<u32>>();
}
