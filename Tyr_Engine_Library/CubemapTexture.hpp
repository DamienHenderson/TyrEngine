#pragma once



#include <GL/glew.h>
#include <string>


#include "Types.hpp"

class CubemapTexture
{
public:
	CubemapTexture(const std::string& dir,const std::string& PosX,const std::string& NegX , const std::string& PosY, const std::string& NegY, const std::string& PosZ, const std::string& NegZ);
	~CubemapTexture();

	bool Load();

	void Bind(GLenum texture_unit);
private:
	std::string file_names[6];
	GLuint texture_id;

	GLenum types[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_X,GL_TEXTURE_CUBE_MAP_NEGATIVE_X,GL_TEXTURE_CUBE_MAP_POSITIVE_Y,GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,GL_TEXTURE_CUBE_MAP_POSITIVE_Z,GL_TEXTURE_CUBE_MAP_NEGATIVE_Z };
};

