#pragma once

#include "Common.h"

namespace Tyr
{
	class System
	{
	public:
		System();
		~System();

		virtual void Update(float delta_time) = 0;
	};

}

