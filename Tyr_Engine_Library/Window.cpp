#include "Window.hpp"


namespace Tyr
{
	Window::Window(u32 width, u32 height,const std::string& title, GLFWmonitor* monitor, GLFWwindow* share,bool vsync) : width_(width) , height_(height)
	{
		window_ = glfwCreateWindow(width, height, title.c_str(), monitor, share);
		glfwSetInputMode(window_, GLFW_STICKY_KEYS, GLFW_TRUE);
		MakeCurrent();
		if (vsync)
		{
			glfwSwapInterval(1);
		}
		else
		{
			glfwSwapInterval(0);
		}
		
		/*
		auto key_callback = [](GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			if (action == GLFW_PRESS)
			{
				std::cout << "Key Pressed: " << key << " with scancode " << scancode << "\n";
			}
			else if (action == GLFW_RELEASE)
			{
				std::cout << "Key Released: " << key << " with scancode " << scancode << "\n";
			}
		};
		*/
		// glfwSetKeyCallback(window_, key_callback);
	}


	Window::~Window()
	{
		glfwDestroyWindow(window_);
		
	}
	void Window::SwapBuffers()
	{
		// std::cout << "Swapped Buffers\n";
		glfwSwapBuffers(window_);
	}
	void Window::MakeCurrent()
	{
		glfwMakeContextCurrent(window_);
	}
	bool Window::ShouldClose()
	{
		return glfwWindowShouldClose(window_);
	}

	GLFWwindow* Window::GetWindowPointer()
	{
		return window_;
	}

}
