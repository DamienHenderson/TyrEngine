#include "GL_Shader.hpp"

GLuint GL_Shader::Program()
{
	return program_;
}

GL_Shader::GL_Shader(const GLchar * vertexPath, const GLchar * fragmentPath)
{
	// 1. Retrieve the vertex/fragment source code from filePath
	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;
	// ensures ifstream objects can throw exceptions:
	vShaderFile.exceptions(std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::badbit);
	try
	{
		// Open files
		vShaderFile.open(vertexPath);
		fShaderFile.open(fragmentPath);
		std::stringstream vShaderStream, fShaderStream;
		// Read file's buffer contents into streams
		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();
		// close file handlers
		vShaderFile.close();
		fShaderFile.close();
		// Convert stream into GLchar array
		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}
	const GLchar* vShaderCode = vertexCode.c_str();
	const GLchar* fShaderCode = fragmentCode.c_str();

	// 2. Compile shaders
	GLuint vertex, fragment;
	GLint success;
	GLchar infoLog[512];

	// Vertex Shader
	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &vShaderCode, NULL);
	glCompileShader(vertex);
	// Print compile errors if any
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertex, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	};

	// Similiar for Fragment Shader
	
	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &fShaderCode, NULL);
	glCompileShader(fragment);
	// Print compile errors if any
	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	};
	// Shader Program
	program_ = glCreateProgram();
	glAttachShader(program_, vertex);
	glAttachShader(program_, fragment);
	glLinkProgram(program_);
	// Print linking errors if any
	glGetProgramiv(program_, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(program_, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	else
	{
		std::cout << "Successfully linked shader with ID: " << program_ << std::endl;
	}
	// Delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

GL_Shader::~GL_Shader()
{
}

void GL_Shader::Use()
{
	glUseProgram(program_);
}

void GL_Shader::SetUniform(const std::string & uniform_name, GLfloat value)
{
	GLint value_location = glGetUniformLocation(program_, uniform_name.c_str());

	glUniform1f(value_location, value);
}

void GL_Shader::SetUniform(const std::string & uniform_name, GLint value)
{
	GLint value_location = glGetUniformLocation(program_, uniform_name.c_str());

	glUniform1i(value_location, value);
}

void GL_Shader::SetUniform(const std::string & uniform_name, GLuint value)
{
	GLint value_location = glGetUniformLocation(program_, uniform_name.c_str());

	glUniform1ui(value_location, value);
}

void GL_Shader::SetUniform(const std::string & uniform_name, glm::vec3 value)
{
	GLint value_location = glGetUniformLocation(program_, uniform_name.c_str());

	glUniform3f(value_location, value.x,value.y,value.z);
}

void GL_Shader::SetUniform(const std::string & uniform_name, glm::mat4 value)
{
	GLint value_location = glGetUniformLocation(program_, uniform_name.c_str());

	glUniformMatrix4fv(value_location,1,GL_FALSE, glm::value_ptr(value));
}

void GL_Shader::SetUniform(const std::string & uniform_name, const PointLight & point_light)
{
	
	SetUniform(uniform_name + ".position",  point_light.position);
	SetUniform(uniform_name + ".constant",  point_light.constant);
	SetUniform(uniform_name + ".linear",    point_light.linear);
	SetUniform(uniform_name + ".quadratic", point_light.quadratic);
	SetUniform(uniform_name + ".ambient",   point_light.ambient);
	SetUniform(uniform_name + ".diffuse",   point_light.diffuse);
	SetUniform(uniform_name + ".specular",  point_light.diffuse);

}

void GL_Shader::SetUniform(const std::string & uniform_name, const DirLight & directional_light)
{
	SetUniform(uniform_name + ".direction", directional_light.direction);
	SetUniform(uniform_name + ".ambient",   directional_light.ambient);
	SetUniform(uniform_name + ".diffuse",   directional_light.diffuse);
	SetUniform(uniform_name + ".specular",  directional_light.specular);
}

void GL_Shader::SetUniform(const std::string & uniform_name, const SpotLight & spot_light)
{
	SetUniform(uniform_name + ".position",      spot_light.position);
	SetUniform(uniform_name + ".direction",     spot_light.direction);
	SetUniform(uniform_name + ".cut_off",       spot_light.cut_off);
	SetUniform(uniform_name + ".outer_cut_off", spot_light.outer_cut_off);
	SetUniform(uniform_name + ".constant",      spot_light.constant);
	SetUniform(uniform_name + ".linear",        spot_light.linear);
	SetUniform(uniform_name + ".quadratic",     spot_light.quadratic);
	SetUniform(uniform_name + ".ambient",       spot_light.ambient);
	SetUniform(uniform_name + ".diffuse",       spot_light.diffuse);
	SetUniform(uniform_name + ".specular",      spot_light.diffuse);
}
