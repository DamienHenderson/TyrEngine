#include "SceneNode.hpp"


namespace Tyr
{


	SceneNode::SceneNode(const std::string& name, std::shared_ptr<Entity>& entity) : name_(name)
	{
		entity_ = entity;
	}


	SceneNode::~SceneNode()
	{
	}

	void SceneNode::AddChild(const std::string & name, std::shared_ptr<Entity>& entity)
	{
		auto pair = std::pair<std::string, std::shared_ptr<SceneNode>>(name, std::make_shared<SceneNode>(name, entity));
		children_.emplace(pair);
	}
	bool SceneNode::HasChildWithName(const std::string & name, bool recurse)
	{
		for (auto& iter : children_)
		{
			if (iter.first == name)
			{
				return true;
			}
			else if (recurse)
			{
				if (iter.second->HasChildWithName(name, recurse))
				{
					return true;
				}
			}
		}
		return false;
	}
	std::map<std::string, std::shared_ptr<SceneNode>>& SceneNode::GetChildren()
	{
		return children_;
	}
}
