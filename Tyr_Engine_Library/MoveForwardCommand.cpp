#include "MoveForwardCommand.hpp"



MoveForwardCommand::MoveForwardCommand(Entity* entity) : Command(entity)
{
}


MoveForwardCommand::~MoveForwardCommand()
{
}

void MoveForwardCommand::execute()
{
	
	auto transform = entity_->GetComponent<TransformComponent>();
	transform->SetTranslation(glm::vec3(0.0f, 0.0f, 1.0f));
}

void MoveForwardCommand::undo()
{
	auto transform = entity_->GetComponent<TransformComponent>();
	transform->SetTranslation(glm::vec3(0.0f, 0.0f, -1.0f));
}