#pragma once
#include <GL\glew.h>

#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


#include "Common.h"
#include "Light.hpp"
class GL_Shader
{
public:
	GLuint Program();
	GL_Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	~GL_Shader();
	void Use();
	void SetUniform(const std::string& uniform_name, GLfloat value);
	void SetUniform(const std::string& uniform_name, GLint value);
	void SetUniform(const std::string& uniform_name, GLuint value);
	void SetUniform(const std::string& uniform_name, glm::vec3 value);
	void SetUniform(const std::string& uniform_name, glm::mat4 value);
	void SetUniform(const std::string& uniform_name, const PointLight& point_light);
	void SetUniform(const std::string& uniform_name, const DirLight&   directional_light);
	void SetUniform(const std::string& uniform_name, const SpotLight& spot_light);
private:
	GLuint program_;
};



