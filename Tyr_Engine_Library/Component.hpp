#pragma once
#include "Common.h"


class Entity;

enum class ComponentType
{
	Base,
	Renderable,
	Controller,
	Transform,
	Collider,
	Count
};
class Component
{
public:
	
	Component(Entity* parent_entity);
	Component(const Component& to_copy);
	virtual ~Component();
	virtual std::string GetDebugString() const;

	virtual Component* Clone()
	{
		return new Component(*this);
	}
	virtual ComponentType GetType() const;
	
	void SetParent(Entity* new_parent);
	Entity* GetParent() { return parent_entity_; }
	
	static constexpr auto Type = ComponentType::Base;
protected:
	Entity* parent_entity_;

private:
	
};

