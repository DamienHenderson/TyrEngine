#pragma once
// External Library Headers

#include "Common.h"


#include <GL/glew.h>





#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



#include <sol/sol.hpp>

#include <NativeFileDialog/nfd.h>



#include <GLFW/glfw3.h>

#include <iomanip>

// Engine Headers
#include "LuaExposedInterface.hpp"
#include "Types.hpp"
#include "Utils.hpp"

#include "GL_Shader.hpp"
#include "Camera.hpp"
#include "Model.hpp"
#include "Light.hpp"
#include "Entity.hpp"
#include "TransformComponent.hpp"
#include "RenderableComponent.hpp"
#include "ControllerComponent.hpp"
#include "Scene.hpp"
#include "Input.hpp"
#include "MoveForwardCommand.hpp"
#include "MoveRightCommand.hpp"
#include "CubemapTexture.hpp"
#include "MoveXYZCommand.hpp"

#include "RenderingSystem.hpp"

#include "Skybox.hpp"

#include "FrameBuffer.hpp"

#include "ColliderComponent.hpp"

#include "Window.hpp"

#include "Font.hpp"

#include "GUI.hpp"

namespace Tyr
{
	class Game : public LuaExposedInterface
	{
	public:


		Game(const std::string& lua_config_filename);
		virtual ~Game();
		virtual int Run();


	private:

		std::unique_ptr<Font> font_;
		
		
		std::unique_ptr<Tyr::Window> window_{ nullptr };

		const double                 update_interval_{ 1.0 / 60.0 };
		double                       time_since_last_update_{ 0.0 };
		
		// tgui::Label::Ptr fps_counter_label{ nullptr };
		Skybox*                     skybox_;
		Entity                      m4_sherman_mesh;

		RenderingSystem             renderer_;
		Scene scene_;
		

		char				        terminal_text_line_buffer[255];
		// background
		// sf::Color(100, 149, 237, 255);
		float						last_time_{ 0.0 };
		
		double						last_mouse_x, last_mouse_y;

		char                        windowTitle[255] = "Tyr Engine";



		bool                        isDirty;

		virtual void				update(float delta_time);
		virtual void				render(float delta_time);

		virtual void				process_terminal_command(const std::string & command);


		bool						running_;
		bool						terminal_active;
		bool						cursor_locked;
		float						fps = 1.f;
		u32							num_frames = 0;

		sol::state					game_lua_state;

		virtual void expose_to_lua();

		std::unique_ptr<GL_Shader>  shader_prog;
		std::unique_ptr<GL_Shader>  skybox_shader;
		std::unique_ptr<GL_Shader>  framebuffer_shader;
		std::unique_ptr<GL_Shader>  font_shader;

		Camera camera;

		u32                         frames_since_last_framerate_poll{ 0 };
		double                      time_since_last_framerate_poll{ 0.0 };
		std::string                 frame_rate_counter_text_{ "" };

		std::vector<Command*> command_queue;
		u32 next_command_index = 0;

		float colour[3] = { 0.f,0.f,0.f };

		void GUI(double delta_time);

		void SetupImGuiStyle(glm::vec3 colour_for_text, glm::vec3 colour_for_head, glm::vec3 colour_for_area, glm::vec3 colour_for_body, glm::vec3 colour_for_pops);

		

		char image_viewer_buf[255];

		bool open_debug_menu = true;
		bool open_demo_window = true;
		bool open_metrics_window = true;



		glm::vec3 move_command_value_;

		GLfloat quadVertices[24] = {   // Vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
									 // Positions   // TexCoords
			-1.0f,  1.0f,  0.0f, 1.0f,
			-1.0f, -1.0f,  0.0f, 0.0f,
			1.0f, -1.0f,  1.0f, 0.0f,

			-1.0f,  1.0f,  0.0f, 1.0f,
			1.0f, -1.0f,  1.0f, 0.0f,
			1.0f,  1.0f,  1.0f, 1.0f
		};

		GLuint quadVAO, quadVBO;

		std::unique_ptr<FrameBuffer> screen_fbo;
	};

}
