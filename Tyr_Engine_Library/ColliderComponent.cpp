#include "ColliderComponent.hpp"






ColliderComponent::~ColliderComponent()
{
}

bool ColliderComponent::Intersects(ColliderComponent * other)
{
	// checks the 2 pointers this needs to see if they are valid and if any of them are not does not try to access which would otherwise cause a segfault
	return (bounding_volume_ && other && bounding_volume_->getAABB().contains(other->bounding_volume_->getAABB()));

}

bool ColliderComponent::Contains(const glm::vec3 & point)
{
	// checks the pointer first and if it isn't valid it returns false without causing a segfault
	return (bounding_volume_ && bounding_volume_->getAABB().contains(reactphysics3d::Vector3(point.x,point.y,point.z)));
}

std::string ColliderComponent::GetDebugString() const
{
	std::stringstream out_ss;

	if (bounding_volume_ == nullptr)
	{
		return "No Collider data present";
	}
	else
	{

	}
}
