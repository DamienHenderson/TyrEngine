#pragma once
#include "Common.h"
#include "Types.hpp"
/// File definition for Map File
/// a Biary file format for writing map data which this game will be able to read
struct MAPFileHeader
{
	// To verify it is the correct type of file
	char identifier[4] = "MAP";
	// Width and height of the map
	u32 width;
	u32 height;
	// The tile definitions for this map
	// const char * Tiledefs_file;
};

void write_MAP(const std::string& filename, const std::vector<u32>& map_data,u32 width,u32 height);

std::vector<std::vector<u32>> read_MAP(const std::string& filename);