#pragma once
#include <GL/glew.h>

#include <glm/glm.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H


#include "Common.h"
#include "Utils.hpp"
#include "GL_Shader.hpp"

//TODO: store characters in an atlas not in individual textures
//TODO: cache static text into textures to speed up rendering
struct Character
{
	GLuint     TextureID;  // ID handle of the glyph texture
	glm::ivec2 Size;       // Size of glyph
	glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
	GLuint     Advance;    // Offset to advance to next glyph
};
class Font
{
public:
	Font(const char* filename);
	~Font();
	void RenderText(GL_Shader &s, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

private:
	FT_Library lib_;
	FT_Face face_;
	std::map<GLchar, Character> characters_;
	GLuint VAO, VBO;
	
	
};

