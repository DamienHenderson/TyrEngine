#include "Game.hpp"





Tyr::Game::Game(const std::string& lua_config_filename) // : skybox_("res/skyboxes/", "ashcanyon_rt.tga", "ashcanyon_lf.tga", "ashcanyon_up.tga", "ashcanyon_dn.tga", "ashcanyon_bk.tga", "ashcanyon_ft.tga")
{

	
	game_lua_state.open_libraries(sol::lib::base);

	game_lua_state.script_file(lua_config_filename);

	u32 window_width = game_lua_state["config"]["window"]["width"];
	u32 window_height = game_lua_state["config"]["window"]["height"];

	std::string title = game_lua_state["config"]["window"]["title"];

	bool fullscreen = game_lua_state["config"]["window"]["fullscreen"];
	bool vsync = game_lua_state["config"]["window"]["vsync"];
	// std::string icon_path = game_lua_state["config"]["window"]["iconpath"];

	u32 bpp = game_lua_state["config"]["window"]["bpp"];

	u32 major_version = game_lua_state["config"]["graphics"]["opengl"]["major"];
	u32 minor_version = game_lua_state["config"]["graphics"]["opengl"]["minor"];
	u32 depth_bits = game_lua_state["config"]["graphics"]["opengl"]["depth_bits"];
	u32 stencil_bits = game_lua_state["config"]["graphics"]["opengl"]["stencil_bits"];


	
	glewExperimental = GL_TRUE;
	
	if (!glfwInit())
	{
		assert("GLFW Init failed" && false);
	}

	glfwSetErrorCallback([](int code, const char* msg) {std::cout << "GLFW Error " << code << ": " << msg << "\n"; });
	running_ = true;

	terminal_active = false;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major_version);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor_version);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
	glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);
	

	u32 count;
	GLFWmonitor** monitors = glfwGetMonitors((int*)&count);

	for (u32 i = 0; i < count; i++)
	{
		GLFWmonitor* monitor = monitors[i];
		const char* monitor_name = glfwGetMonitorName(monitor);
		u32 physical_width, physical_height;
		glfwGetMonitorPhysicalSize(monitor, (int*)&physical_width, (int*)&physical_height);
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		double dpi = mode->width / (physical_width / 25.4);
		std::cout << "Monitor " << i << ": " << monitor_name << " with dimensions of " << physical_width << "mm x " << physical_height << "mm and DPI of " << dpi << "\n";
	}
	if (fullscreen)
	{
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		
		
		window_ = std::make_unique<Tyr::Window>(mode->width, mode->height, title, monitor, nullptr, vsync);
		
	}
	else
	{
		
		window_ = std::make_unique<Tyr::Window>(window_width, window_height, title, nullptr, nullptr, vsync);
	}
	
	window_->MakeCurrent();

	if (glewInit() != GLEW_OK)
	{
		assert("Glew Init failed!" && false);
	}

	// std::cout << "OpenGL version " << game_window.getSettings().majorVersion << "." << game_window.getSettings().minorVersion << std::endl;
	// std::cout << "OpenGL info: " << std::endl << "Version: " << glGetString(GL_VERSION) << std::endl << "Vendor: " << glGetString(GL_VENDOR) << std::endl << "Renderer: " << glGetString(GL_RENDERER) << std::endl;


	glViewport(0, 0, window_->GetWidth(), window_->GetHeight());
	GLErrorCheck();
	// glOrtho(0, game_window.getSize().x, 0, game_window.getSize().y, 0, 1);
	// gluPerspective(90.f, 1.f, 1.f, 300.f);

#ifdef _DEBUG
	GLErrorCheck();
	std::cout << "retreiving max texture units\n";
	GLint texture_units;
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &texture_units);

	std::cout << "Max Texture Units: " << texture_units << std::endl;
#endif

	this->isDirty = true;

	shader_prog   = std::make_unique<GL_Shader>("res/shaders/textured_lit.vert", "res/shaders/textured_lit.frag");
	skybox_shader = std::make_unique<GL_Shader>("res/shaders/skybox.vert", "res/shaders/skybox.frag");
	framebuffer_shader = std::make_unique<GL_Shader>("res/shaders/render_framebuffer.vert", "res/shaders/render_framebuffer.frag");
	font_shader = std::make_unique<GL_Shader>("res/shaders/font_shader.vert", "res/shaders/font_shader.frag");

	screen_fbo = std::make_unique<FrameBuffer>(window_->GetWidth(), window_->GetHeight());

	skybox_ = new Skybox("res/skyboxes/Mountains/", "right.jpg", "left.jpg", "top.jpg", "bottom.jpg", "back.jpg", "front.jpg");

	

	

	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	

	

	/*
	m4_sherman_mesh.AddComponent<TransformComponent>(glm::mat4());
	if (!m4_sherman_mesh.HasComponent<TransformComponent>())
	{
		std::cout << "M4 Sherman does not have transform component" << std::endl;
		assert(false);
	}
	m4_sherman_mesh.GetComponent<TransformComponent>()->SetScale(glm::vec3(0.05));
	m4_sherman_mesh.GetComponent<TransformComponent>()->SetTranslation(glm::vec3(0.0f, 0.0f, 20.0f));

	m4_sherman_mesh.AddComponent<RenderableComponent>("res/meshes/M4.obj");





	scene_.AddEntity("M4 Sherman", m4_sherman_mesh);

	*/

	std::shared_ptr<Entity> freighter = std::make_shared<Entity>();
	// Freighter.AddComponent<TransformComponent>(glm::mat4());
	freighter->SetTransform(glm::mat4());
	freighter->AddComponent<RenderableComponent>("res/meshes/Freighter/Si-Fi_Freigther/OBJ/Freigther_BI_Export.obj");
	// Freighter.AddComponent<ColliderComponent>());

	
	scene_.AddEntity("Freighter", freighter);

	
	// std::shared_ptr<Entity> zenith = std::make_shared<Entity>();
	// zenith->SetTransform(glm::rotate(glm::mat4(), 30.0f, glm::vec3(1.0f)));
	// zenith->AddComponent<RenderableComponent>("res/meshes/Zenith/Zenith_OBJ.obj");

	
	auto& freighter_node = scene_.GetNode("Freighter");
	// scene_.AddEntity(freighter_node,"Zenith", zenith);
	std::shared_ptr<Entity> street = std::make_shared<Entity>();
	glm::mat4 scale_matrix = glm::scale(glm::mat4(), glm::vec3(0.05f));
	street->SetTransform(scale_matrix);
	street->AddComponent <RenderableComponent>("res/meshes/sci_fi_street/scifi_street_mall/scifi_street_mall.obj");

	scene_.AddEntity(freighter_node, "Street", street);

	DirLight light;
	light.direction = glm::vec3(-0.2f, -1.0f, -0.3f);
	light.diffuse = glm::vec3(0.05f, 0.05f, 0.05f);
	light.ambient = glm::vec3(0.4f, 0.4f, 0.4f);
	light.specular = glm::vec3(0.5f, 0.5f, 0.5f);


	scene_.AddDirLight(light);

	glm::vec3 pointLightPositions[] =
	{
		glm::vec3(0.7f,  0.2f,  2.0f),
		glm::vec3(2.3f, -3.3f, -4.0f),
		glm::vec3(-4.0f,  2.0f, -12.0f),
		glm::vec3(0.0f,  0.0f, -3.0f)
	};


	PointLight point_light;

	point_light.position = pointLightPositions[0];
	point_light.ambient = glm::vec3(0.05f, 0.05f, 0.05f);
	point_light.diffuse = glm::vec3(0.8f, 0.8f, 0.8f);
	point_light.specular = glm::vec3(1.0f, 1.0f, 1.0f);
	point_light.constant = 1.0f;
	point_light.linear = 0.09;
	point_light.quadratic = 0.032;

	scene_.AddPointLight(point_light);

	point_light.position = pointLightPositions[1];
	point_light.diffuse = glm::vec3(0.0f, 1.0f, 0.3f);
	scene_.AddPointLight(point_light);

	point_light.position = pointLightPositions[2];
	point_light.diffuse = glm::vec3(1.0f, 0.0f, 0.3f);
	scene_.AddPointLight(point_light);

	point_light.position = pointLightPositions[3];
	point_light.diffuse - glm::vec3(0.0f, 0.0f, 1.0f);
	scene_.AddPointLight(point_light);


	SpotLight spot_light;
	spot_light.position = camera.Position;
	spot_light.direction = camera.Front;
	spot_light.ambient = glm::vec3(0.0f, 0.0f, 0.0f);
	spot_light.diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	spot_light.specular = glm::vec3(1.0f, 1.0f, 1.0f);
	spot_light.constant = 1.0f;
	spot_light.linear = 0.09;
	spot_light.quadratic = 0.032;
	spot_light.cut_off = glm::cos(glm::radians(12.5f));
	spot_light.outer_cut_off = glm::cos(glm::radians(15.0f));

	scene_.AddSpotLight(spot_light);

	

	glm::vec3 text_colour(game_lua_state["config"]["gui"]["theme"]["text"]["r"], game_lua_state["config"]["gui"]["theme"]["text"]["g"], game_lua_state["config"]["gui"]["theme"]["text"]["b"]);
	glm::vec3 head_colour(game_lua_state["config"]["gui"]["theme"]["head"]["r"], game_lua_state["config"]["gui"]["theme"]["head"]["g"], game_lua_state["config"]["gui"]["theme"]["head"]["b"]);
	glm::vec3 area_colour(game_lua_state["config"]["gui"]["theme"]["area"]["r"], game_lua_state["config"]["gui"]["theme"]["area"]["g"], game_lua_state["config"]["gui"]["theme"]["area"]["b"]);
	glm::vec3 body_colour(game_lua_state["config"]["gui"]["theme"]["body"]["r"], game_lua_state["config"]["gui"]["theme"]["body"]["g"], game_lua_state["config"]["gui"]["theme"]["body"]["b"]);
	glm::vec3 pops_colour(game_lua_state["config"]["gui"]["theme"]["pops"]["r"], game_lua_state["config"]["gui"]["theme"]["pops"]["g"], game_lua_state["config"]["gui"]["theme"]["pops"]["b"]);

	SetupImGuiStyle(text_colour, head_colour, area_colour, body_colour, pops_colour);

	

	
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glfwGetCursorPos(window_->GetWindowPointer(), &last_mouse_x, &last_mouse_y);
	
	font_ = std::make_unique<Font>("res/fonts/Terminus.ttf");

	ImGui_ImplGlfwGL3_Init(window_->GetWindowPointer(), true);
	
}


Tyr::Game::~Game()
{
	
	glfwTerminate();
	
	
	
}

int Tyr::Game::Run()
{
	update(0.0f);
	while (running_)
	{

		
		num_frames++;
		frames_since_last_framerate_poll++;
		double frametime = glfwGetTime() - last_time_;
		time_since_last_framerate_poll += frametime;
		last_time_ = glfwGetTime();
		
		/*
		if (num_frames % 60 == 0)
		{
			std::cout << frametime << "\n";
		}
		*/
		
		if (time_since_last_framerate_poll >= 1.0)
		{
			std::stringstream ss;
			ss << "FPS: " << frames_since_last_framerate_poll << "\ntesting newlines \t and tabs";
			
			time_since_last_framerate_poll = 0.0;
			frames_since_last_framerate_poll = 0;
			frame_rate_counter_text_ = ss.str();
		}
		
		
		

		time_since_last_update_ += frametime;
		
		if (time_since_last_update_ >= update_interval_)
		{
			update(frametime);
		}
		
		
		
			
		
		
		render(frametime);
		GLErrorCheck();
		// std::cout << "FPS: " << 1.0 / frametime << "\n";
		running_ = !window_->ShouldClose();
		
		
		 
		

	}
	return 0;
}



void Tyr::Game::update(float delta_time)
{
	

	if (next_command_index < command_queue.size())
	{
		command_queue[next_command_index]->execute();
		next_command_index++;
	}
	if (glfwGetKey(window_->GetWindowPointer(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window_->GetWindowPointer(), GLFW_TRUE);
	}
	if (glfwGetKey(window_->GetWindowPointer(), GLFW_KEY_W) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(FORWARD, fabs(delta_time));
		
	}
	if (glfwGetKey(window_->GetWindowPointer(), GLFW_KEY_A) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(LEFT, fabs(delta_time));

	}
	if (glfwGetKey(window_->GetWindowPointer(), GLFW_KEY_S) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(BACKWARD, fabs(delta_time));

	}
	if (glfwGetKey(window_->GetWindowPointer(), GLFW_KEY_D) == GLFW_PRESS)
	{
		camera.ProcessKeyboard(RIGHT, fabs(delta_time));

	}
	double xpos, ypos;
	glfwGetCursorPos(window_->GetWindowPointer(), &xpos, &ypos);

	double mouse_delta_x = xpos - last_mouse_x;
	double mouse_delta_y = last_mouse_y - ypos;

	last_mouse_x = xpos;
	last_mouse_y = ypos;

	camera.ProcessMouseMovement(mouse_delta_x, mouse_delta_y);

	scene_.GetSpotLight(0).direction = camera.Front;


	fps = 1.f / (float)delta_time;

	scene_.GetSpotLight(0).position = camera.Position;

	float movement_value = constant_motion(0.0f, 0.5f, glfwGetTime());

	glm::mat4 translation_matrix = glm::translate(glm::mat4(), glm::vec3(movement_value));
	auto& freighter = scene_.GetNode("Freighter");
	if (freighter != nullptr)
	{
		auto weak_ref = freighter->GetEntityPtr();
		if (!weak_ref.expired())
		{
			auto strong_ref = weak_ref.lock();
			strong_ref->SetTransform(translation_matrix);
		}
		
	}
	GUI(delta_time);

	
}

void Tyr::Game::render(float delta_time)
{
	
	window_->MakeCurrent();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);
	/// OpenGL
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


#ifdef _DEBUG
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#endif


	// Camera/View transformation
	glm::mat4 view;
	view = camera.GetViewMatrix();
	// Projection 
	glm::mat4 projection;
	projection = glm::perspective(camera.Zoom, (GLfloat)window_->GetWidth() / (GLfloat)window_->GetHeight(), 0.1f, 1000.0f);


	screen_fbo->Bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.61f, 0.79f, 0.66f, 1.0f);
	

	skybox_->Render(*skybox_shader, camera, projection);

	shader_prog->Use();

	shader_prog->SetUniform("view", view);
	shader_prog->SetUniform("projection", projection);

	

	shader_prog->SetUniform("viewPos", camera.Position);


	
	scene_.Render(renderer_, *shader_prog);

	glUseProgram(0);

	screen_fbo->UnBind();
	glClear(GL_COLOR_BUFFER_BIT);
	
	


	
	glDisable(GL_DEPTH_TEST); // We don't care about depth information when rendering a single quad

	// glDisable(GL_FRAMEBUFFER_SRGB);

#ifdef _DEBUG
	// glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
#endif 
	framebuffer_shader->Use();

	
	 glBindVertexArray(quadVAO);
	
	 glActiveTexture(GL_TEXTURE0);
	 glBindTexture(GL_TEXTURE_2D, screen_fbo->GetTextureID());	// Use the color attachment texture as the texture of the quad plane
	 glEnableVertexAttribArray(0);
	 glEnableVertexAttribArray(1);
	 glDrawArrays(GL_TRIANGLES, 0, 6);
	 glDisableVertexAttribArray(0);
	 glDisableVertexAttribArray(1);
	 glBindTexture(GL_TEXTURE_2D,0);
	 glBindVertexArray(0);
	 glUseProgram(0);

	 
	 font_shader->Use();
	 glm::mat4 ortho_projection = glm::ortho(0.0f, (float)window_->GetWidth(), 0.0f, (float)window_->GetHeight() );
	 font_shader->SetUniform("projection", ortho_projection);
	 font_->RenderText(*font_shader, frame_rate_counter_text_, 0.0f, 1032.0f, 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	

	

	ImGui::Render();
	
	window_->SwapBuffers();
	glfwPollEvents();
	// this->isDirty = false;
}

void Tyr::Game::process_terminal_command(const std::string & command)
{
	if (command == "quit\n" || command == "Quit\n")
	{
		// game_window.close();

		running_ = false;
	}
	else
	{

		game_lua_state.do_string(command);



	}
}

void Tyr::Game::expose_to_lua()
{
	return;
}


void Tyr::Game::GUI(double  delta_time)
{
	ImGui_ImplGlfwGL3_NewFrame();
	if (open_debug_menu)
	{
		ImGui::Begin("Debug Menu", &open_debug_menu, ImGuiWindowFlags_HorizontalScrollbar); // begin window
																							// Window title text edit
		

		if (ImGui::CollapsingHeader("Performance", ImGuiTreeNodeFlags_Bullet))
		{
			ImGui::Text("Frametime %f ms", delta_time * 1000.0f);

			
			
			
			// ImGui::TreePop();
		}
		if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_Bullet))
		{
			ImGui::Text("Orientation: \n\tYaw: %f \n\tPitch: %f", camera.Yaw, camera.Pitch);
			ImGui::Text("Position:    \n\tX: %f \n\tY: %f \n\tZ: %f", camera.Position.x, camera.Position.y, camera.Position.z);
			// ImGui::TreePop();
		}


		if (ImGui::CollapsingHeader("Debug Options", ImGuiTreeNodeFlags_Bullet))
		{
			if (ImGui::Button("Quit"))
			{
				ImGui::OpenPopup("Quit?");
			}
			if (ImGui::BeginPopupModal("Quit?", NULL, ImGuiWindowFlags_AlwaysAutoResize))
			{
				ImGui::Text("Are you sure you want to quit?\n\n");
				ImGui::Separator();
				if (ImGui::Button("OK", ImVec2(120, 0)))
				{
					ImGui::CloseCurrentPopup();
					
					running_ = false;
				}
				ImGui::SameLine();
				if (ImGui::Button("Cancel", ImVec2(120, 0)))
				{
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}

			// ImGui::TreePop();
		}
		
		
		
		
		if (ImGui::CollapsingHeader("Scene", ImGuiTreeNodeFlags_Bullet))
		{
			for (auto& name : scene_.GetEntityNames())
			{
				if (ImGui::CollapsingHeader(name.c_str()))
				{
					auto& node = scene_.GetNode(name);
					if (node == nullptr)
					{
						break;
					}
					for (auto& iter : node->GetChildren())
					{
						if (ImGui::CollapsingHeader(iter.first.c_str()))
						{

						}
					}
				}
			}
			/*
			for (u32 i = 0; i < scene_.GetPointLightCount(); i++)
			{
				auto& temp = scene_.GetPointLight(i);
				std::string light_str = "PointLight" + std::to_string(i);
				if(ImGui::CollapsingHeader(light_str.c_str()))
				{
					ImGui::TextColored(ImVec4(temp.position.x, temp.position.y, temp.position.z,1.0f),"Position: %f %f %f", temp.position.x, temp.position.y, temp.position.z);
					ImGui::TextColored(ImVec4(temp.ambient.x, temp.ambient.y, temp.ambient.z, 1.0f), "Ambient: %f %f %f", temp.ambient.r, temp.ambient.g, temp.ambient.b);
					ImGui::TextColored(ImVec4(temp.diffuse.x, temp.diffuse.y, temp.diffuse.z, 1.0f), "Diffuse: %f %f %f", temp.diffuse.r, temp.diffuse.g, temp.diffuse.b);
					ImGui::TextColored(ImVec4(temp.specular.x, temp.specular.y, temp.specular.z, 1.0f), "Specular: %f %f %f", temp.specular.r, temp.specular.g, temp.specular.b);
					ImGui::Text("Constant: %f", temp.constant);
					ImGui::Text("Linear: %f", temp.linear);
					ImGui::Text("Quadratic: %f", temp.quadratic);

				}
			}
			for (u32 i = 0; i < scene_.GetDirLightCount(); i++)
			{
				auto& temp = scene_.GetDirLight(i);
				std::string light_str = "DirectionalLight" + std::to_string(i);
				if (ImGui::CollapsingHeader(light_str.c_str()))
				{
					ImGui::TextColored(ImVec4(temp.direction.x, temp.direction.y, temp.direction.z, 1.0f), "Direction: %f %f %f", temp.direction.x, temp.direction.y, temp.direction.z);
					ImGui::TextColored(ImVec4(temp.ambient.x, temp.ambient.y, temp.ambient.z, 1.0f), "Ambient: %f %f %f", temp.ambient.r, temp.ambient.g, temp.ambient.b);
					ImGui::TextColored(ImVec4(temp.diffuse.x, temp.diffuse.y, temp.diffuse.z, 1.0f), "Diffuse: %f %f %f", temp.diffuse.r, temp.diffuse.g, temp.diffuse.b);
					ImGui::TextColored(ImVec4(temp.specular.x, temp.specular.y, temp.specular.z, 1.0f), "Specular: %f %f %f", temp.specular.r, temp.specular.g, temp.specular.b);
					

				}
			}
			for (u32 i = 0; i < scene_.GetSpotLightCount(); i++)
			{
				auto& temp = scene_.GetSpotLight(i);
				std::string light_str = "SpotLight" + std::to_string(i);
				if (ImGui::CollapsingHeader(light_str.c_str()))
				{
					ImGui::TextColored(ImVec4(temp.position.x, temp.position.y, temp.position.z,1.0f),"Position: %f %f %f", temp.position.x, temp.position.y, temp.position.z);
					ImGui::TextColored(ImVec4(temp.direction.x, temp.direction.y, temp.direction.z, 1.0f), "Direction: %f %f %f", temp.direction.x, temp.direction.y, temp.direction.z);
					ImGui::TextColored(ImVec4(temp.ambient.x, temp.ambient.y, temp.ambient.z, 1.0f), "Ambient: %f %f %f", temp.ambient.r, temp.ambient.g, temp.ambient.b);
					ImGui::TextColored(ImVec4(temp.diffuse.x, temp.diffuse.y, temp.diffuse.z, 1.0f), "Diffuse: %f %f %f", temp.diffuse.r, temp.diffuse.g, temp.diffuse.b);
					ImGui::TextColored(ImVec4(temp.specular.x, temp.specular.y, temp.specular.z, 1.0f), "Specular: %f %f %f", temp.specular.r, temp.specular.g, temp.specular.b);
					ImGui::Text("Constant: %f", temp.constant);
					ImGui::Text("Linear: %f", temp.linear);
					ImGui::Text("Quadratic: %f", temp.quadratic);
					ImGui::Text("Cutoff: %f", temp.cut_off);
					ImGui::Text("Outer Cutoff: %f", temp.outer_cut_off);

				}
			}
			*/

		}
		

		ImGui::End(); // end window
	}


	// if (open_metrics_window)
	// {
	// 	ImGui::ShowMetricsWindow(&open_metrics_window);
	// }
	// if (open_demo_window)
	// {
	//	ImGui::ShowTestWindow(&open_demo_window);
	// }
	// ImGui::ShowStyleEditor();

	// ImGui::ShowUserGuide();

	ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
	ImGui::Begin("Performance", false, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar);
	

	// frame time
	if (delta_time <= 0.0166f)
	{
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Frame time: %.4f", delta_time);
	}
	else if (delta_time <= 0.0333f && delta_time >= 0.0166f)
	{
		ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "Frame time: %.4f", delta_time);
	}
	else
	{
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Frame time: %.4f", delta_time);
	}
	ImGui::End();
	/*
	if (terminal_active)
	{
		ImGui::Begin("Terminal");
		ImGui::Text(std::string(terminal_text_history).c_str());
		if (ImGui::InputText("", terminal_text_line_buffer, 255, ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_AllowTabInput))
		{
			process_terminal_command(terminal_text_line_buffer);
			terminal_text_history += "\n";
			terminal_text_history += terminal_text_line_buffer;
			for (int i = 0; i < 255; i++)
			{
				terminal_text_line_buffer[i] = '\0';
			}
		}

		ImGui::End();
	}
	*/
}


void Tyr::Game::SetupImGuiStyle(glm::vec3 colour_for_text, glm::vec3 colour_for_head, glm::vec3 colour_for_area, glm::vec3 colour_for_body, glm::vec3 colour_for_pops)
{
	ImGuiStyle& style = ImGui::GetStyle();

	style.Colors[ImGuiCol_Text] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 1.00f);
	style.Colors[ImGuiCol_TextDisabled] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 0.58f);
	style.Colors[ImGuiCol_WindowBg] = ImVec4(colour_for_body.x, colour_for_body.y, colour_for_body.z, 0.95f);
	style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 0.58f);
	style.Colors[ImGuiCol_Border] = ImVec4(colour_for_body.x, colour_for_body.y, colour_for_body.z, 0.00f);
	style.Colors[ImGuiCol_BorderShadow] = ImVec4(colour_for_body.x, colour_for_body.y, colour_for_body.z, 0.00f);
	style.Colors[ImGuiCol_FrameBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 1.00f);
	style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.78f);
	style.Colors[ImGuiCol_FrameBgActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_TitleBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 1.00f);
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 0.75f);
	style.Colors[ImGuiCol_TitleBgActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_MenuBarBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 0.47f);
	style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.21f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.78f);
	style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_ComboBg] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 1.00f);
	style.Colors[ImGuiCol_CheckMark] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.80f);
	style.Colors[ImGuiCol_SliderGrab] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.50f);
	style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_Button] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.50f);
	style.Colors[ImGuiCol_ButtonHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.86f);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_Header] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.76f);
	style.Colors[ImGuiCol_HeaderHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.86f);
	style.Colors[ImGuiCol_HeaderActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_Column] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.32f);
	style.Colors[ImGuiCol_ColumnHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.78f);
	style.Colors[ImGuiCol_ColumnActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_ResizeGrip] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.15f);
	style.Colors[ImGuiCol_ResizeGripHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.78f);
	style.Colors[ImGuiCol_ResizeGripActive] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_CloseButton] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 0.16f);
	style.Colors[ImGuiCol_CloseButtonHovered] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 0.39f);
	style.Colors[ImGuiCol_CloseButtonActive] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 1.00f);
	style.Colors[ImGuiCol_PlotLines] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 0.63f);
	style.Colors[ImGuiCol_PlotLinesHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_PlotHistogram] = ImVec4(colour_for_text.x, colour_for_text.y, colour_for_text.z, 0.63f);
	style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 1.00f);
	style.Colors[ImGuiCol_TextSelectedBg] = ImVec4(colour_for_head.x, colour_for_head.y, colour_for_head.z, 0.43f);
	style.Colors[ImGuiCol_PopupBg] = ImVec4(colour_for_pops.x, colour_for_pops.y, colour_for_pops.z, 0.92f);
	style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(colour_for_area.x, colour_for_area.y, colour_for_area.z, 0.73f);

	style.ChildWindowRounding = 0.0f;
	style.FrameRounding = 0.0f;
	style.GrabRounding = 0.0f;
	style.ScrollbarRounding = 0.0f;
	style.WindowRounding = 0.0f;
}












