#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

#include <string>
#include <sstream>

#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cstddef>

#include <memory>

#include <vector>
#include <map>
#include <queue>
#include <unordered_map>
#include <array>

#include <functional>

#include <thread>