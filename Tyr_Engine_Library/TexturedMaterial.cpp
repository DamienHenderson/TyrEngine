#include "TexturedMaterial.hpp"



TexturedMaterial::TexturedMaterial(const std::vector<Texture>& textures , float shininess) : Material(glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(0.0f),shininess) , textures_(textures)
{
	
}


TexturedMaterial::~TexturedMaterial()
{
}

void TexturedMaterial::ApplyToShader(GL_Shader & shader) const
{
	shader.Use();

	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	GLuint normalNr = 1;
	for (GLuint i = 0; i < this->textures_.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);

		GLenum err = glGetError();
		if (err != GL_NO_ERROR)
		{
			std::cout << err << std::endl;
		}
		std::stringstream ss;
		std::string number;
		std::string name = this->textures_[i].type;
		if (name == "texture_diffuse")
		{
			ss << diffuseNr++; // Transfer GLuint to stream
		}
		else if (name == "texture_specular")
		{
			ss << specularNr++; // Transfer GLuint to stream
		}
		else if (name == "texture_normal")
		{
			ss << normalNr++;
		}
		number = ss.str();

		shader.SetUniform(name + number, (GLint)i);
		
		glBindTexture(GL_TEXTURE_2D, this->textures_[i].id);


	}
	shader.SetUniform("shininess", shininess_);
}


