#pragma once
#include "Material.hpp"
#include "Types.hpp"
class TexturedMaterial : public Material
{
public:
	TexturedMaterial(const std::vector<Texture>& textures , float shininess);
	~TexturedMaterial();
	virtual void ApplyToShader(GL_Shader& shader) const override;
private:
	const std::vector<Texture> textures_;
};

