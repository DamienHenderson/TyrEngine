#pragma once
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "Common.h"
#include "Types.hpp"
#include "GL_Shader.hpp"
#include "TexturedMaterial.hpp"
class Mesh
{
public:
	Mesh();
	Mesh(const std::vector<GL_Vertex>& vertices, const std::vector<GLuint>& indices, const std::vector<Texture>& textures,const std::string& name = "",float shininess = 0.0f);
	~Mesh();
	
	
	void Draw(GL_Shader& shader);

	const std::string& GetName();
	const u32 GetFaces();
	const u32 GetVertices();
private:
	std::vector<GL_Vertex> vertices_;
	std::vector<GLuint> indices_;
	TexturedMaterial material_;

	std::string name_;

	u32 faces_count, vertices_count;

	

	/*  Render data  */
	GLuint VAO, VBO, EBO;
	/*  Functions    */
	void setupMesh();

	

	bool first_run;

	// std::fstream log;
};

