#pragma once

#include "Types.hpp"
#include "GL_Util.hpp"

class FrameBuffer
{
public:
	FrameBuffer(u32 width,u32 height);
	~FrameBuffer();
	u32 GetWidth() { return width_; }
	u32 GetHeight() { return height_; }

	GLuint GetID() { return id; }
	GLuint GetTextureID() { return tex_id; }

	void Bind();
	void UnBind();
private:
	u32 width_{ 0 }, height_{ 0 };
	GLuint id;
	GLuint rbo_id;
	GLuint tex_id;
};

