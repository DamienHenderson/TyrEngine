#pragma once
#include <GL\glew.h>

#include "Common.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "GL_Shader.hpp"
#include "Types.hpp"
#include "Mesh.hpp"
#include "GL_Util.hpp"


class Model
{
public:
	Model();
	Model(const std::string & filepath);
	~Model();
	void Draw(GL_Shader & shader);
	void Load(const std::string& filepath);
	const std::string& GetName();
private:
	/*  Model Data  */
	std::vector<Mesh> meshes;
	std::string directory;
	std::string name_;
	/*  Functions   */
	void loadModel(const std::string& path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type,const std::string& typeName);
};

