#pragma once

#include "Common.h"
#include "Model.hpp"
#include "Types.hpp"
#include "Font.hpp"
#include "Material.hpp"
#include "TexturedMaterial.hpp"
class AssetManager
{
public:
	AssetManager();
	~AssetManager();

private:
	std::unordered_map<std::string, Model> models_;
	std::unordered_map<std::string, Texture> textures_;
	std::unordered_map<std::string, Font> fonts_;

};

