#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Types.hpp"
namespace Tyr
{
	class Window
	{
	public:
		/// Creates a new window with the provided arguments
		/// pass nullptr for monitor and/or share if you do not care about them
		Window(u32 width, u32 height, const std::string& title,GLFWmonitor* monitor = nullptr, GLFWwindow* share = nullptr, bool vsync = false);
		~Window();

		u32 GetWidth() const { return width_; }
		u32 GetHeight() const { return height_; }
		void SwapBuffers();
		void MakeCurrent();
		bool ShouldClose();

		GLFWwindow* GetWindowPointer();
		
	private:
		u32 width_, height_;
		GLFWwindow* window_;
	};
}
