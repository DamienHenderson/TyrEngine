#pragma once
#include "Common.h"

#include <GL/glew.h>

#include <GLFW/glfw3.h>



#include "Component.hpp"
#include "Entity.hpp"
#include "Input.hpp"

#include "Command.hpp"


class ControllerComponent : public Component
{
public:
	
	ControllerComponent(Entity* parent);
	~ControllerComponent();
	
	void ProcessInput(u32 action);

	 void SetAction(Command* command, u32 action);
	
	static constexpr auto Type = ComponentType::Controller;

	virtual ComponentType GetType() const override { return Type; };
	
protected:
	// TODO: make the Action an enum class
	// TODO: make an enum class with all sfml input types to serve as the map key
	std::array< Command*, GLFW_KEY_LAST> control_actions_array_;


	virtual void process_action(u32 action);

};

