#include "Skybox.hpp"



Skybox::Skybox(const std::string& dir, const std::string& PosX, const std::string& NegX, const std::string& PosY, const std::string& NegY, const std::string& PosZ, const std::string& NegZ) : cubemap(dir,  PosX,  NegX,  PosY,  NegY, PosZ, NegZ)
{
	Load();
}


Skybox::~Skybox()
{
}

void Skybox::Render(GL_Shader & skybox_shader,Camera& camera, const glm::mat4& projection)
{
	glDisable(GL_FRAMEBUFFER_SRGB);
	// disable writing to the depth buffer
	glDisable(GL_DEPTH_TEST);
	// glDepthMask(GL_FALSE);
	skybox_shader.Use();
	glm::mat4 view = glm::mat4(glm::mat3(camera.GetViewMatrix()));
	skybox_shader.SetUniform("view", view);
	skybox_shader.SetUniform("projection", projection);

	glBindVertexArray(skyboxVAO);
	cubemap.Bind(GL_TEXTURE0);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	// glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);
}

void Skybox::Load()
{
	glGenVertexArrays(1, &this->skyboxVAO);
	glGenBuffers(1, &this->skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glBindVertexArray(0);
}
