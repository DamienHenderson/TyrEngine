#pragma once

#include <glm/glm.hpp>

#include "Command.hpp"

/// Move XYZ Relative
class MoveXYZCommand : public Command
{
public:
	MoveXYZCommand(Entity* entity,glm::vec3 new_pos);
	virtual ~MoveXYZCommand();

	virtual void execute();

	virtual void undo();
private:
	glm::vec3 destination, previous{ 0.0f };
};

