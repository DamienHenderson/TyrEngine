#include "RenderableComponent.hpp"






RenderableComponent::RenderableComponent(Entity* parent,const std::string & model_filename) : Component(parent), model_(model_filename)
{
	
}

RenderableComponent::~RenderableComponent()
{
}

void RenderableComponent::Render(GL_Shader& shader)
{
	
	// std::cout << this->GetDebugString() << ": Attempting Render." << std::endl;
	
	model_.Draw(shader);
}
