#pragma once

#include "Common.h"
#include <glm/glm.hpp>
#include "GL_Shader.hpp"

class Material
{
public:
	Material(const glm::vec3& diffuse, const glm::vec3& ambient, const glm::vec3& specular,float shininess);
	~Material();

	/// sets the values of "diffuse" , "ambient" , "specular" and "shininess" in the shader passed to this function
	virtual void ApplyToShader(GL_Shader& shader) const;
protected:
	glm::vec3 diffuse_;
	glm::vec3 ambient_;
	glm::vec3 specular_;
	float     shininess_;
};

