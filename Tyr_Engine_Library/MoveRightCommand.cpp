#include "MoveRightCommand.hpp"



MoveRightCommand::MoveRightCommand(Entity* entity) : Command(entity)
{
}


MoveRightCommand::~MoveRightCommand()
{
}

void MoveRightCommand::execute()
{
	auto transform = entity_->GetComponent<TransformComponent>();
	transform->SetTranslation(glm::vec3(1.0f, 0.0f, 0.0f));
}

void MoveRightCommand::undo()
{
	auto transform = entity_->GetComponent<TransformComponent>();
	transform->SetTranslation(glm::vec3(-1.0f, 0.0f, 0.0f));
}
