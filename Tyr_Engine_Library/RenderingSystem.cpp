#include "RenderingSystem.hpp"

namespace Tyr
{



	RenderingSystem::RenderingSystem()
	{
	}


	RenderingSystem::~RenderingSystem()
	{
	}

	void RenderingSystem::Update(float delta_time)
	{

	}

	void RenderingSystem::Render(Entity & entity,GL_Shader& shader, const glm::mat4& parent_transform) const
	{
		
			if (entity.HasComponent<RenderableComponent>())
			{
				/*
				if (entity.HasComponent<TransformComponent>())
				{
					shader.SetUniform("model", entity.GetComponent<TransformComponent>()->GetTransform());
				}
				*/
				// just to make sure
				shader.Use();
				auto& combined_transform = entity.GetTransform() * parent_transform;
				shader.SetUniform("model", combined_transform);//GetComponent<TransformComponent>()->GetTransform());
				auto& renderable = entity.GetComponent<RenderableComponent>();
				if (renderable == nullptr)
				{
					return;
				}
				else
				{
					renderable->Render(shader);
				}
			}
		
	}

}