#pragma once
#include "System.hpp"
#include "Common.h"



#include "Utils.hpp"

namespace Tyr
{
	
	class InputSystem : public Tyr::System
	{
	public:
		InputSystem();
		~InputSystem();
		virtual void Update(float delta_time) override;
		bool GetKey(const_string_ref key);
		bool GetKeyByAlias(const_string_ref alias);
		bool GetMouseButton(const_string_ref button);
		void AddKeyAlias(const_string_ref alias, const_string_ref key);
		void AddMouseAlias(const_string_ref alias, const_string_ref key);
	private:
		std::map<std::string, bool> keys_;
		std::map<std::string, bool> mouse_buttons_;
		// set by users
		std::map<std::string, std::string> key_aliases_;
		std::map<std::string, std::string> mouse_button_aliases_;


	};
}

