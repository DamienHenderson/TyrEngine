#include "GL_Util.hpp"



GLint TextureFromFile(const std::string& path, const std::string& directory)
{
	// std::cout << directory << path << std::endl;
	
	

	std::string filename = path;
	filename = directory + '/' + filename;

	std::cout << "Loading " << filename << " as an OpenGL Texture." << std::endl;
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	Tyr::ImageData data = Tyr::ImageFromFile(filename);

	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data.width, data.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
	{
		GLfloat fLargest;

		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

GLint sRGBA_TextureFromFile(const std::string & path, const std::string & directory)
{
	

	std::string filename = path;
	filename = directory + '/' + filename;

	std::cout << "Loading " << filename << " as an sRGBA OpenGL Texture." << std::endl;
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	Tyr::ImageData data = Tyr::ImageFromFile(filename);
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glGetError();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data.width, data.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.pixels);
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		std::cout << "OpenGL Error: " << err << "\n";
	}
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
	{
		GLfloat fLargest;

		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}
void GLErrorCheck()
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		std::cout << "OpenGL Error: " << err << "\n";
	}
}
void glUniformVec3(GL_Shader & shader, GLint location, const glm::vec3 uniform)
{
	shader.Use();

	glUniform3f(location, uniform.x, uniform.y, uniform.z);


}


