REM generates a folder called Redist in the current working directory
REM this folder can be redistributed and the executable within will run
del /s /q Redist_X64\*.*
rd Redist_X64
md Redist_X64
md Redist_X64\Licenses
md Redist_X64\res
REM TODO: get 64 bit builds working
copy Release\Demo.exe Redist_X64\Redist64.exe
copy Release\*.dll Redist_X64\
copy Tyr\imgui.ini Redist_X64\
Robocopy /E .\Tyr\res .\Redist_X64\res