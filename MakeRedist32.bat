REM generates a folder called Redist in the current working directory
REM this folder can be redistributed and the executable within will run
del /s /q Redist_X86\*.*
rd Redist_X86
md Redist_X86
md Redist_X86\Licenses
md Redist_X86\res
REM 
copy Release\Demo.exe Redist_X86\Redist32.exe
copy Release\*.dll Redist_X86\
copy Tyr\imgui.ini Redist_X86\
Robocopy /E .\Tyr\res .\Redist_X86\res