#version 330

#define NUM_POINT_LIGHTS 4


struct PointLight
{
    vec3 position;

	float constant;
	float linear;
	float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct DirLight
{
	vec3 direction;

	vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct SpotLight 
{
    vec3 position;
    vec3 direction;
    float cut_off;
    float outer_cut_off;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;       
};




in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in mat3 TBN;



out vec4 colour;

// Texture samplers

// Diffuse colour
uniform sampler2D texture_diffuse1;

// Specular maps
uniform sampler2D texture_specular1;

// Normal maps
uniform sampler2D texture_normal1;

// Lighting
uniform vec3 viewPos;
uniform float shininess;
uniform PointLight point_lights[NUM_POINT_LIGHTS];
uniform DirLight   directional_light;
uniform SpotLight  spot_light;



vec3 CalcDirLight  (DirLight light  ,  vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light,  vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight (SpotLight light ,  vec3 normal, vec3 fragPos, vec3 viewDir);
void main()
{
	float gamma = 2.2;
	
	vec3 norm = texture(texture_normal1,TexCoord).rgb;
	norm = normalize(norm * 2.0 - 1.0);
	
	vec3 view_dir = normalize(viewPos - FragPos);
	

	vec3 result = CalcDirLight(directional_light,norm,view_dir);
	for(int i = 0; i < NUM_POINT_LIGHTS; i++)
	{
        result += CalcPointLight(point_lights[i], norm, FragPos, view_dir);   
	}
	result += CalcSpotLight(spot_light, norm, FragPos, view_dir); 

	colour = vec4(result,1.0);
    // colour.rgb = pow(colour.rgb, vec3(1.0/gamma));
	
	

	
	
}

// Calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(texture_diffuse1, TexCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, TexCoord));
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoord));
    return (ambient + diffuse + specular);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(texture_diffuse1, TexCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, TexCoord));
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoord));
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

// Calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir =  normalize(light.position - fragPos);
	vec3 halfwayDir = normalize(lightDir + viewDir);
    // Diffuse shading
    float diff = max(dot(normal, halfwayDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // Spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cut_off - light.outer_cut_off;
    float intensity = clamp((theta - light.outer_cut_off) / epsilon, 0.0, 1.0);
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(texture_diffuse1, TexCoord));
    vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, TexCoord));
    vec3 specular = light.specular * spec * vec3(texture(texture_specular1, TexCoord));
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular);
}
