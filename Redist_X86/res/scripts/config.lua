config = 
{
	window = 
	{
		width = 1920,
		height = 1080,
		title = "Tyr Engine",
		fullscreen = false,
		bpp = 32,
		vsync = false,
		-- no icon
		iconpath = ""
	},
	graphics = 
	{
		opengl = 
		{
			major = 4,
			minor = 5,
			depth_bits = 24,
			stencil_bits = 8
		}
	},
	keybindings =
	{
		{
			"Action" , "Key"
		}
	},
	gui =
	{
		font = "res/fonts/Roboto-Regular.ttf",
		theme =
		{
			text = 
			{
				r = 1.0,
				g = 1.0,
				b = 1.0

			},
			head =
			{
				r = 0.0,
				g = 0.475,
				b = 0.42
				
				
			},
			area =
			{
				r = 0.0,
				g = 0.376,
				b = 0.392
			},
			body =
			{
				r = 0.259,
				g = 0.259,
				b = 0.259
			},
			pops =
			{
				r = 0.259,
				g = 0.259,
				b = 0.259
			}
		}
	}
}