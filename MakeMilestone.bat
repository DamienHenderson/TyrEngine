IF "%~1"=="" GOTO USAGE
echo "%~1"
rd %1
md %1
md %1\Source
copy Tyr_Engine_Library\*.cpp %1\Source\
copy Tyr_Engine_Library\*.hpp %1\Source\
copy Tyr_Engine_Library\*.h %1\Source\
copy Tyr_Engine_Library\*.txt %1\
copy *.txt %1\
goto DONE
:USAGE
	echo "usage: MakeMilestone.bat <desired name of milestone folder>"
:DONE
		