#version 330 core
in vec2 TexCoords;
out vec4 colour;

uniform sampler2D screenTexture;

// does not invert alpha channel
vec4 Invert(vec4 in_colour)
{
	return vec4(vec3(1.0 - in_colour.rgb), in_colour.a);
}
float Luminance(vec4 in_colour )
{
    return (colour.r + colour.g + colour.b ) / 3.0;
}
float ScaledLuminance(vec4 in_colour)
{
	return dot(vec3(0.30, 0.59, 0.11), in_colour.rgb);
}
vec4 GreyScale(vec4 in_colour)
{
	float average = ScaledLuminance(in_colour);
	return vec4(average,average,average,1.0);
}

vec4 Sepia( vec4 in_colour )
{
    return vec4(
          clamp(in_colour.r * 0.393 + in_colour.g * 0.769 + in_colour.b * 0.189, 0.0, 1.0)
        , clamp(in_colour.r * 0.349 + in_colour.g * 0.686 + in_colour.b * 0.168, 0.0, 1.0)
        , clamp(in_colour.r * 0.272 + in_colour.g * 0.534 + in_colour.b * 0.131, 0.0, 1.0)
        , in_colour.a
    );
}
vec4 Sharpen(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

	/*
    float kernel[9] = float[](
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
    );
	*/
	float kernel[9] = float[](
	     0,-1,0,
	    -1,5,-1,
	     0,-1,0
	);

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 Blur(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
    1.0 / 16, 2.0 / 16, 1.0 / 16,
    2.0 / 16, 4.0 / 16, 2.0 / 16,
    1.0 / 16, 2.0 / 16, 1.0 / 16  
);

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 EdgeDetection(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
        1, 1, 1,
        1,  -8, 1,
        1, 1, 1
    );

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 EdgeEnhance(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
        0, 0, 0,
        -1,  1, 0,
        0, 0, 0
    );

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 Emboss(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
        -2, -1, 0,
        -1,  1, 1,
         0,  1, 2
    );

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 BottomSobel(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
        -1, -2,-1,
         0,  0, 0,
         1,  2, 1
    );

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 Outline(vec2 tex_coord)
{
	float offset = 1.0/ 300;
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),  // top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

    float kernel[9] = float[](
        -1, -1, -1,
        -1,  8, -1,
        -1, -1, -1
    );

	 vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(screenTexture, tex_coord.st + offsets[i]));
    }
    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
	{
		col += sampleTex[i] * kernel[i];
	}
     return vec4(col,1.0);  
}
vec4 Posterise(vec2 tex_coord)
{
	float gamma = 0.6;
	float num_colours = 8.0;
	vec3 c = texture(screenTexture, tex_coord).rgb;	
	c = pow(c, vec3(gamma, gamma, gamma));
    c = c * num_colours;
    c = floor(c);
    c = c / num_colours;
    c = pow(c, vec3(1.0/gamma));
	return vec4(c,1.0);
}
void main()
{ 
	// no post processing
    colour = texture(screenTexture, TexCoords);
	
	// colour = Invert(colour);

	// very bad may be broken
	// colour = Greyscale(colour);

	// colour = Sepia(colour);
	
	// float L = ScaledLuminance(colour);
	// colour.rgb = vec3(L,L,L); 
	
	// colour = Sharpen(TexCoords);

	// colour = Blur(TexCoords);

	// colour = EdgeDetection(TexCoords);

	// colour = EdgeEnhance(TexCoords);

	// colour = Emboss(TexCoords);

	// colour = BottomSobel(TexCoords);

	// colour = Outline(TexCoords);

	// colour = Posterise(TexCoords);
}