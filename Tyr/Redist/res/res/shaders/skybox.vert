#version 330

layout (location = 0) in vec3 Position;
out vec3 TexCoords;

uniform mat4 projection;
uniform mat4 view;


void main()
{
    gl_Position = projection * view * vec4(Position, 1.0);  
    TexCoords = Position;
	
	// do not use, causes sphincter of death
	// TexCoords.y = 1.0 - TexCoords.y;
}  