#version 330

in vec3 TexCoords;

out vec4 FragColour;

uniform samplerCube skybox;

void main()
{
    FragColour = texture(skybox, TexCoords);
	// FragColour.rgb = pow(FragColour.rgb, vec3(1.0/2.2));
}