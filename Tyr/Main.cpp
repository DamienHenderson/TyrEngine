#include <tyr.hpp>



#ifndef _DEBUG
#	ifdef _WIN32
#		pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#	endif // 
#endif




int main()
{
	
	


	std::unique_ptr<Tyr::Game> game = std::make_unique<Tyr::Game>("res/scripts/config.lua");
	int result = game->Run();

	if ( result )
	{
		// Optional error printing here
	}
	return result;
}
